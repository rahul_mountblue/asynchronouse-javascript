const boards = require('./data/boards.json');
// Problem 1: Write a function that will return a particular board's
// information based on the boardID that is passed from the given list
// of boards in boards.json and then pass control back to the code that 
// called it by using a callback function.

// find board data on behalf of their id

function infoFromThanosBoards(boardPara) {
    return new Promise((resolve,reject)=> {

        setTimeout(()=>{
            let result = boards.find((info)=>{
                if(info.id === boardPara || info.name === boardPara) {
                    return true;
                }
            })
    
            if(result !== undefined) {
                    resolve(result);
            } else {
                console.log(`${boardId} does not exist in boards.json`);
            }
             
        },2000)
    })
}


module.exports = {
    infoFromThanosBoards  
}