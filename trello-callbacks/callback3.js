const list = require('./data/lists.json');
const cards = require('./data/cards.json');
/* 
	Problem 3: Write a function that will return all cards that 
    belong to a particular list based on the listID that
    is passed to it from the given data in cards.json.
    Then pass control back to the code that called it by using a callback function.
*/

function returnAllCardUsingListId(listId) {
return new Promise((resolve,reject)=> {

    setTimeout(()=> {
            if(listId !== null || listId !== undefined) {
                resolve(cards[listId])
            } else {
                reject("listId can not be null or undefined");
            }
        },2000)
   })
}

module.exports = {
    returnAllCardUsingListId
}

// returnAllCardUsingListId("qwsa221",function(data){
//     let result = printAllCards(data)
//     console.log(result);
// })