const callback3 = require('../callback3');
const printData = require('../printData');


// testing and calling

let testArr = [
    {
      id: '1',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '2',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '3',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    }
  ]

  // calling a function and send control to other code using callback

  callback3.returnAllCardUsingListId("qwsa221")
  .then((cardData)=> printData.printDataAsPromise(cardData))
  .then((data)=>{
    let response = testArr.filter((item,index,oriArr)=>{
      if(oriArr[index].description === data[index].description) {
                  return true;
      }
    })
    if(response[0]!== undefined) {
      console.log(data);
    }

  })
  .catch(err=>console.log(err))