const callback1 = require('../callback1');
const printData = require('../printData')


// for testing
let testObj = {
    "id": "mcu453ed",
    "name": "Thanos",
    "permissions": {
        
    }
}

callback1.infoFromThanosBoards("mcu453ed")
.then((boardInfo)=>printData.printDataAsPromise(boardInfo))
.then((data)=>{
    if(JSON.stringify(data) === JSON.stringify(testObj)) {
        console.log(data);
    }
})
.catch(err=>console.log(err))