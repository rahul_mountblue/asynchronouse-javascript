const callback2 = require('../callback2');
const printData = require('../printData');
const callback1 = require('../callback1');


// calling and testing 
let testArr = [
    {
        "id": "qwsa221",
        "name": "Mind"
    },
    {
        "id": "jwkh245",
        "name": "Space"
    },
    {
        "id": "azxs123",
        "name": "Soul"
    },
    {
        "id": "cffv432",
        "name": "Time"
    },
    {
        "id": "ghnb768",
        "name": "Power"
    },
    {
        "id": "isks839",
        "name": "Reality"
    }
]

callback1.infoFromThanosBoards("mcu453ed")
.then((boardInfo)=> callback2.returnList(boardInfo))
.then((listData)=>printData.printDataAsPromise(listData))
.then((data)=>{
    let flag = false;
        testArr.forEach((item,index,oriArr)=>{
                if(JSON.stringify(oriArr[index]) === JSON.stringify(data[index])) {
                    flag = true;
                } else {
                    flag = false;
                }
        })
        if(flag) {
            console.log(data);
        }       
})
.catch(err=>console.log(err))
