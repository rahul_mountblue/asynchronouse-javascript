const callback4 = require('../callback4');
const callback1 = require('../callback1');

// testing and calling

let testArr = [
    {
      id: '1',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '2',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '3',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    }
  ];

  function test(data) {
    return new Promise((resolve,reject)=>{

      let respones = data.filter((data,index,arr)=>{
        if(arr[index].description === testArr[index].description ) {
            return true;
        }
    })
    if(respones[0] !== undefined) {
        resolve(data);
    }
    })
}

// calling functions

  callback1.infoFromThanosBoards("Thanos")
  .then((boardData)=> callback4.listForParticularBoards(boardData))
  .then((listForThanosBoard)=> callback4.allCardForMindList(listForThanosBoard))
  .then((data)=> test(data))
  .then((result)=> console.log(result))
  .catch(err=>console.log(err))
