const callback5 = require('../callback5');
const callback1 = require('../callback1');
const callback4 = require('../callback4');

// testing and calling

let testArr = [
    {
      id: '1',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '2',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '3',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    }
  ];

let testText = "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar";

// calling function and testing 

callback1.infoFromThanosBoards("Thanos")
.then((boardData)=> callback4.listForParticularBoards(boardData))
.then((listForThanosBoard)=> callback5.allCardForMindList(listForThanosBoard))
.then((result)=>{
     console.log(result);
})
.catch(err=> console.log(err))

    
    