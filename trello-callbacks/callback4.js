const lists = require('./data/lists.json');
const cards = require('./data/cards.json');

/* 
	Problem 4: Write a function that will use the previously written
     functions to get the following information. You do not need to pass control 
     back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
// pass name of the board
// function for getting all list related thanos board

function listForParticularBoards(boardData) {

    return new Promise((resolve,reject)=>{

        setTimeout(()=> {
            if( boardData !== null || boardData !== undefined) {
                resolve(lists[boardData.id]);
            } else {
                reject("board data can not be null or undefined");
            }
        },2000)
    })
}
// function get all cards for Mind

function allCardForMindList(listOfThanosBoard) {
    return new Promise((resolve,reject)=>{

        setTimeout(function() {

            let mindList = listOfThanosBoard.filter((data)=> data.name === 'Mind');
            let allCards = cards[mindList[0].id];
            resolve(allCards);
         },2000)
         
    })
   
}

module.exports = {
    listForParticularBoards,
    allCardForMindList
}