const list = require('./data/lists.json');
/* 
	Problem 2: Write a function that will return all lists that 
    belong to a board based on the boardID that is passed to it from 
    the given data in lists.json. Then pass control back to the code that 
    called it by using a callback function.
*/

function returnList(result) {
    return new Promise((resolve,reject)=>{
        
        setTimeout(()=>{

            if(result !== null || result !== undefined) {
                resolve(list[result.id]);
            }  else {
                reject("result data can not be null or undefined");
            }          
        },3*1000)
    })
        
}

module.exports = {
   returnList,   
}