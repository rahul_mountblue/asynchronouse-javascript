
function printDataAsPromise(data) {
    return new Promise((resolve,reject)=>{
        setTimeout(function(){
            resolve(data);
        },2*1000)
        
    })
}

module.exports = {
    printDataAsPromise,
}