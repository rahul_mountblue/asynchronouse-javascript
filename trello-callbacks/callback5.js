const cards = require('./data/cards.json');
const callback3 = require('./callback3');

/* 
	Problem 5: Write a function that will use the previously 
    written functions to get the following information.
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

// function get all cards for Mind and Space

function allCardForMindList(listOfThanosBoard) {
    return new Promise((resolve,reject)=>{

        setTimeout(function() {

            let mindList = listOfThanosBoard.filter((data)=> data.name === 'Mind' || data.name === 'Space');
            let promiseArr = [];
            mindList.forEach((data)=>{

               if(data.id !== undefined && cards[data.id] !== undefined) {
                  promiseArr.push(callback3.returnAllCardUsingListId(data.id))
               }
            })

             let res =  Promise.all(promiseArr);
             
             resolve(res);
         },2000)
    })
}

module.exports = {
    allCardForMindList
}