const fs = require('fs');

// create diractory name randomJson

function createDir(noOfFiles,cb) {

    let path = './randomJson';
    
    fs.mkdir('./randomJson', (err)=> {
        if(err) {
            throw err;
        } else {
            cb(err,path);
        } 

    });
}

// create no of random files define by noOfFiles argument

function createRandomJsonFiles(noOfFiles, path,cb) {

    while(noOfFiles--) {

        let number = Math.random()*10;
        fs.writeFile(`${path}/file${number}.json`,'',(err)=> {
            if(err) {
                throw err;
            } 
        })
    }
    cb(path);
}

// remove all the file generated randomly

function removeAllFiles(path, cb) {

    fs.readdir(path,(err,files)=> {
         
        if(err) {
            throw error;
        } else {
            files.forEach((file)=> {
                console.log(file);

                fs.unlink(`${path}/${file}`, err => {
                    if(err) throw err;
                })
            })
            
         }
        cb(err,path);
    })
}

function readDirlength(path,cb) {
    let dirLen;
    fs.readdir(path,(err,files)=> {
        dirLen = files.length;
        cb(dirLen);
    })
    
}

module.exports = {
    createDir,
    createRandomJsonFiles,
    removeAllFiles,
    readDirlength
}


