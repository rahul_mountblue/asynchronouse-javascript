const fs = require('fs');

//read given lipsum.txt and convert file data into uppercase and write into new file
const fileNameArr = [];
function readGivenFile(path) {
     let newFileName = 'newFile1.txt';

    return new Promise((resolve,reject)=> {

        fs.readFile(`${path}`,'utf-8', (err,data)=> {
            if(err) {
                reject(err);
            } else {
    
                fs.writeFile(`../data/${newFileName}`,data.toUpperCase(),(err)=>{
                    if(err) {
                        reject(err);
                    } else {
                        fileNameArr.push(newFileName);
                        resolve(newFileName)
                    }
                })
            }
    })
    })
    
}

// read data from first new file and convert into lower case, split the content into sentence
// and write into second new file

function readSpliteAndWrite(file) {
    let newFileName2 = 'newFile2.txt';

    return new Promise((resolve,reject)=> {
        fs.readFile(`../data/${file}`,'utf-8',(err,data)=> {
            if(err) {
                reject(err);
            } else {

                let loCaseData = data.toLowerCase().split('.').join('\n');

            fs.writeFile(`../data/${newFileName2}`,loCaseData,(err)=>{
                if(err) {
                    reject(err);
                } else {
                    fileNameArr.push(newFileName2);
                    resolve(newFileName2); 
                }
            })

        }
           
    })

    })
    
}

// read second new file and sort the  content

function readFileAndSortContent(file) {
    let newFileName3 = 'newFile3.txt';
    return  new Promise((resolve,reject)=> {
        fs.readFile(`../data/${file}`,'utf-8',(err,data)=> {
            if(err) {
                reject(err);
            } else {
                let sortedData = data.split(' ').sort().join(' ');
    
                fs.writeFile(`../data/${newFileName3}`,`${sortedData} `,err=> {
                    if(err) {
                        reject(err);
                    } else {
                        fileNameArr.push(newFileName3);
                        resolve(newFileName3)
                    }
                })
                
            }
        })
    })
    
}

// read filenames.txt file and delete all new files generated from above operations

function readAndDelete(){
    return new Promise((resolve,reject)=> {

        fs.readFile('../data/filenames.txt','utf-8',(err,data)=> {
            fileNameData = data.trim().split(' ');
            
            fileNameData.forEach((file)=> {
    
                fs.readdir('../data',(err, files)=> {
                    
                    if(err) {
                        reject(err);
                    } else {
                        files.forEach((file2)=> {
                            if(file === file2) {
                                fs.unlink(`../data/${file2}`,(err)=> {
                                    //console.log(err);
                                })                            
                            }
                        })
                        resolve();
                    }
                })
            })
            
        })
    })
}

// store file name into filenames.txt

function storeFileNames() {
    return new Promise((resolve,reject)=>{
        let file = fileNameArr.join(' ');
        fs.appendFile('../data/filenames.txt',`${file} `,(err)=> {
            if(err) {
                reject(err);
            } else {
                resolve();
            }
        })
    })

}

function getErr() {
    console.log('something went wrong');
}

module.exports = {
    readGivenFile,
    readSpliteAndWrite,
    readFileAndSortContent,
    readAndDelete,
    storeFileNames,
    getErr

}